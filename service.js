"use strict";
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const conexion = {
    "database": "cecati",
    "username": "informatica",
    "pwd": "informatica",
    "config": {
        "dialect": "mysql",
        "host": "localhost"
};
module.exports = {
	//nombre del microservicio
	name: "Membresia",

	mixins: [DbService],

	adapter: new SqlAdapter(conexion.database, conexion.username, conexion.pwd, conexion.config),
	model: {
		name: "Membresia",//nombre de la base de datos
        define: {
			Nombre: Sequelize.STRING,
			Grupo: Sequelize.STRING
            // content: Sequelize.TEXT,
            // votes: Sequelize.INTEGER,
            // author: Sequelize.INTEGER,
            // status: Sequelize.BOOLEAN
        },
        options: {
            // Options from http://docs.sequelizejs.com/manual/tutorial/models-definition.html
        }
    },
	settings: {
		onError(req, res, err) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(err.code || 500);
            res.end(JSON.stringify({
                success: false,
                message: err.message
            }));
        }	

	},


	/**
	 * Service dependencies
	 */
	//dependencies: [],	

	/**
	 * Actions
	 */
	actions: {

		//funciones GET
		hello() {
			var insert = {
				nombre:"ruben dario",
				apellidos:"zamorano cervantes",
				edad:"26"
			}

			var update = {
				nombre:"ruben dario",
				apellidos:"zamorano cervantes",
				edad:"25"
			}
			// //use query
			// return this.adapter.db.query("select * from alumnos").then(result => {
			// 	return result[0];
			// });

			// //insert from model
			// return this.model.create(insert).then(result => {
			// 	return result[0];
			// });

			// //insert
			// return this.adapter.insert(insert).then(result => {
			// 	return result;
			// });

			// //find
			// return this.adapter.find({edad:"25"}).then(result => {
			// 	return result;
			// });

			// //findById
			// return this.adapter.findById(1).then(result => {
			// 	return result;
			// });

			// //updateById
			// return this.adapter.updateById(4,{$set:update}).then(result => {
			// 	return result;
			// });

			// //updateMany
			// return this.adapter.updateMany({edad:"26"},update).then(result => {
			// 	return result;
			// });

			// //removeById
			// return this.adapter.removeById(1).then(result => {
			// 	return result;
			// });

			// //removeMany
			// return this.adapter.removeMany({nombre:"ruben2"}).then(result => {
			// 	return result;
			// });

			// //list
			// return this.model.findAll({limit:10,offset:0,where:{edad:"25"}}).then(result => {
			// 	return result;
			// });

			// //count
			// return this.adapter.count({}).then(result => {
			// 	return result;
			// });
		},



		ObtenerTodos()
		{
			//list
			return this.model.findAll({limit:10,offset:0,where:{}}).then(result => {
				return result;
			});
		},

		//funciones POST
		ObtenerPorId: {
			params: {
				id: "string"
			},
			handler(ctx) {
				// data:ctx.params
				// //findById
				return this.adapter.findById(parseInt(ctx.params.id)).then(result => {
					return result;
				});
			}
		},
		Agregar: {
			handler(ctx) {
				// data:ctx.params
				//insert
				var insert = ctx.params.objeto;
				return this.adapter.insert(insert).then(result => {
					return result;
				});
			}
		},
		Eliminar: {
			params: {
				id: "string"
			},
			handler(ctx) {
				// data:ctx.params
				//removeById
				console.log(ctx.params);
				return this.adapter.removeById(parseInt(ctx.params.id)).then(result => {
					return result;
				});
				
			}
		},
		Modificar:{
			handler(ctx){
				var update = ctx.params.objeto;
				var id=update.id;
				return this.adapter.updateById(id,{$set:update}).then(result => {
					return result;
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}	
};